#**Theme : Grille d’évaluation du travail du groupe**

##**No. Groupe**
* _Nom et Prénom_
* _Exercice ou question_

##**Item évalué No. 1 :** 
* Travailler en groupe (écouter les autres, s'impliquer, réaliser sa part de travail,  jouer son rôle).
* _Niveau  de maitrise observé_ 
**Insuffisant** : Tous les élèves ne sont pas intégrés au groupe ou les élèves ne travaillent 
................. que     séparément ou les échanges sont hors-sujet.
**Fragile** : Les élèves travaillent ensemble mais les critères3 ne sont pas respectés.
**Satisfaisant** : Les élèves travaillent ensemble en respectant les critères avec l’aide de 
................. l’enseignant, ou respectent la majorité des critères1.
**Très satisfaisant** : Les élèves travaillent ensemble en respectant les critères1 et sans 
...................... l’aide de l’enseignant         

##**Item évalué No. 2 :** 
* Aboutir à une réalisation à plusieurs.
* _Niveau  de maitrise observé_
**Insuffisant** : Le groupe n’a pas produit, n’a établi aucun accord sur une stratégie commune.
**Fragile** :Le groupe a fourni un travail peu avancé ou chaque participant a produit un 
............ travail personnel non terminé.
**Satisfaisant** : La quasi-totalité du groupe a fourni un travail abouti ou le groupe a ...... 
................. fourni un travail presque terminé.
**Très satisfaisant** : Le groupe est tombé d’accord et a réalisé une production aboutie.

##**Item évalué No. 3 :** 
* S’engager dans une démarche en mobilisant des procédures déjà rencontrées et en élaborant un raisonnement adapté.
 * _Niveau  de maitrise observé_
**Insuffisant** : Le groupe ne produit rien seul, il a un besoin constant de l’enseignant.
**Fragile** : Le groupe reconnaît des démarches déjà rencontrées avec l’aide du professeur et 
............ s’y engage correctement. L’aide du professeur est régulière.
**Satisfaisant** : Le groupe reconnaît presque toutes les démarches déjà rencontrées sans 
.................  l’aide du professeur et s’y engage correctement. L’aide du professeur est 
.................  ponctuelle.
**Très satisfaisant** : Le groupe reconnaît toutes les démarches du cours et s’y engage, sans 
......................  aide du professeur.

##**Item évalué No. 4 :** 
* Extraire et organiser l'information utile. 
* _Niveau  de maitrise observé_
**Insuffisant** : Le groupe ne trouve aucune information seul..
**Fragile** : Peu d’informations sont extraites et l’aide du professeur est régulière.
**Satisfaisant** : Les informations sont presque toutes extraites seuls ou toutes le sont avec 
.................  une aide ponctuel
**Très satisfaisant** :   Les informations sont toutes extraites en autonomie.
          

##**Item évalué No. 5 :** 
* Tester plusieurs pistes de résolution. 
* _Niveau  de maitrise observé_
**Insuffisant** : Le groupe ne teste rien y compris après l’aide de l’enseignant
**Fragile** : Peu Le groupe a besoin d’une présence régulière du professeur pour tester des 
............  pistes de résolution
**Satisfaisant** : Le groupe a besoin d’une présence ponctuelle du professeur pour tester des 
.................  pistes de résolution
**Très satisfaisant** : Le groupe est autonome dans sa mise en place de pistes de    
....................... résolution.     

##**Item évalué No. 6 :** 
* Expliquer sa démarche ou son raisonnement, comprendre les explications d'un autre et argumenter dans l'échange
* _Niveau  de maitrise observé_
**Insuffisant** : Aucune réponse n’est donnée ou les réponses ne sont ni rédigées ni expliquées.
**Fragile** : Les réponses sont données et bien rédigées mais les méthodes ne sont pas 
............  expliquées ou le groupe repère les démonstrations à plusieurs étapes et écrit un 
............  début de raisonnement qui n’aboutit pas. 
**Satisfaisant** : Les explications sont incomplètes ou dans l’écrit on peut repérer les 
.................  différentes étapes des démonstrations, mais elles ne sont pas clairement 
.................  identifiées ou ordonnées. 
**Très satisfaisant** : Les explications données sont justes. Dans l’écrit, on peut repérer les 
...................... différentes étapes de démonstration, clairement identifiées et .... 
...................... ordonnées.          

##**Item évalué No. 7 :** 
* Démontrer, utiliser un raisonnement logique et des règles établies pour parvenir à une conclusion.
* _Niveau  de maitrise observé_
**Insuffisant**: Le groupe ne produit aucun raisonnement malgré l’aide de l’enseignant, ou 
...............  refuse l’aide.
**Fragile** : L’aide de l’enseignant est régulière pour pouvoir raisonner.
**Satisfaisant** : L’aide de l’enseignant est ponctuelle pour pouvoir raisonner.. 
**Très satisfaisant** : Le groupe raisonne seul de manière logique en s’appuyant sur des 
......................  propriétés.          


