##**Theme: Evaluation bilan, niveau première.**

Charles Poulmaire propose une évaluation bilan pour des élèves de première.
Cette évaluation comprend trois parties distinctes :
*	un QCM, via GeNumSI 
*	3 exercices de programmation sur des notebooks*
*	3 exercises sur feuille

Le barème est indiqué sur le sujet. Et une grille d'évaluation pour le professeur est proposée.
Nous approprions  cette évaluation et nous proposons dans les lignes qui suivent une analyse de cette évaluation.

**Analyse proposée.**

Il s’agit d’une évaluation bilan réalisée pour les élèves de niveau première. cette evaluation a ete realisee a partir d'un lot d'exercices bien definis. L'auteur a classé ces exercices en trois groupes: 
* Balayage par sonar 
* Drôle de sommes
* Tableau de recherches dans p1

Suite à ce classement, l'auteur a élaboré la grille d’évaluation, tout en tenant compte des niveaux de difficultés de chaque question posée dans les dites exercices.

Ainsi, les exercices inscrits dans le groupe **balayage sonar** sont comptés pour 9 points, les  exercices inscrits dans le groupe **Drôles de sommes** sont comptés pour 11 points, et les exercices inscrits dans le groupe **Tableau de recherche dans p1** sont comptés pour 9 points.

Par ailleurs, dans le groupe **Balayage sonar**, on dénombre deux exercices. L’évaluation a donc été  portée sur ces deux  exercices qui, en tenant compte des questions posées, présentent à peu près les mêmes niveaux de difficultés. Ainsi, le premier exercice est compté pour 4.5 points et le second pour egalement 4.5 points. 

Dans le groupe **Drôles de sommes**, dans lequel on a implémenté deux fonctions utilisant deux méthodes différentes : 
* une méthode calculatoire pour la première et 
* une méthode se basant sur un changement de type pour la seconde,
on dénombre six exercices dont trois exercices sont associés à la première méthode et les trois autres sont associés à la deuxième méthode.

Relative à la première méthode,  l’évaluation a été portée sur les trois exercices. Les exercices 1 et 3  semblent présentés les mêmes niveaux de difficultés sont comptés, chacun, pour deux points ; et l’exercice 2 plus difficile que les deux autres est compté pour  3 points.

En ce qui concerne la deuxième méthode, l’évaluation a été portée sur deux exercices : l’exercice 1 et l’exercice 3. L’exercice 1 est compté pour  un(1) point et l’exercice 3 plus difficiles que l’exercice 1 est compté pour trois points.

Dans le groupe **tableau de recherche dans P1**, on compte quatre exercices, l’évaluation a été portée sur trois de ces quatres exercices : la première, la troisième et la quatrième.  

Le premier exercice et le troisième,  présentant à peu près les mêmes niveaux de difficulté, sont comptés chacun pour deux points et le 4e exercice plus difficiles par rapport aux 1ere et 3e est compté pour cinq (5) points.

A noter que les resultats ainsi obtenus seront traités en vue de répondre à un ensemble de questions posées ou qui seront posées ou qui pourront etre posées dans le cadre de cette évaluation.

Tel est notre analyse de cette évaluation bilan de Charles P.
