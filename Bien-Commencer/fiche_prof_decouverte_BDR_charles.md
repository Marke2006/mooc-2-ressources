**Thématique :** Base de données

**Notions liées :** Données en table de première

**Résumé de l’activité :** Activité débranchée en deux parties. Gestion de données inconvénients et avantages.

**Objectifs :** Découverte des bases de données relationnelles. Introduire le modèle relationnel sans trop de formalisation : relation, attribut, domaine, clef primaire, clef étrangère, schéma relationnel. 

**Auteur :** Charles

**Durée de l’activité :** 2h 

**Forme de participation :** individuelle ou en binôme, en autonomie, collective 

**Matériel nécessaire :** Papier, stylo !

**Préparation :** Aucune

**Autres références :** 

**Fiche élève cours :** 

**Fiche élève activité :** Disponible [ici](https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/fiche_eleve_decouverte_BDR_charles.pdf)   
