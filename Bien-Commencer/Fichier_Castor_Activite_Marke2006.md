
Dans le cadre de cette activite, il nous a demandé de choisir cinq exerices du castor informatique [sur] (**https://castor-informatique.fr/home.php**) et pour chacun de ces exercices nous devrons renseigner sur:
1.	une année
2.	le niveau choisi
3.	l'exercice analysé
4.	3 mots faisant référence aux notions et concepts informatique abordés
5.	si vous le souhaitez, vous pourrez ajouter votre nom et/ou prénom, ou pseudo

Nous présentons notre travail comme suit:

Onglet # 1

Année concours : 2020
Niveau choisi : Terminale
Exercices : L'archipel des Castors
Notions et Concepts : Graphe
Notions et Concepts : Algorithme
Notions et Concepts : Arbre courant
Pseudo : Marke2006

Onglet # 2

Année concours : 2020
Niveau choisi : Première
Exercices : Transport d'argent
Notions et Concepts : Nombres binaires
Notions et Concepts : Applications des nombres binaires en informatiques
Notions et Concepts : Octet
Pseudo : Marke2006

Onglet # 3

Année concours : 2019
Niveau choisi : NS1
Exercices : Garniture de Hamburger
Notions et Concepts : Théorie des ensembles
Notions et Concepts : Bases des données
Notions et Concepts : Algèbre de Boole
Pseudo : Marke2006

Onglet # 4

Année concours : 2017
Niveau choisi : SN1
Exercices : Des téléchargements en parallèle
Notions et Concepts : Bande passante
Notions et Concepts : Ethernet/internet
Notions et Concepts : Barre de progression
Pseudo : Marke2006

Onglet # 5

Année concours : 2018
Niveau choisi : Terminale
Exercices : Classement de livres
Notions et Concepts : Algorithme glouton
Notions et Concepts : Réseau de tri
Notions et Concepts : Tri parallèle
Pseudo : Marke2006

