#**Module Turtle pour introduire Python**
##**Objectifs** 
 * Définir python........................................................................................* Familiariser les élèves avec le langage python
##**Pré-requis à cette activité** .................................................................                   * Mathématiques (Géométrie plane des classes antérieures)....................................................................................* Dessins techniques
##**Durée de l'activité**.........................................................................          * Environ deux  (2) heure.

##**Exercices cibles**........................................................................................* Tracer des formes géométriques: carrés, cristal, hexagone, cercle, décagone, double décagone, triangle, etc. ..........................................................................................* Tracer des formes géométriques simples à l’aide du module turtle (des carres par exemple) ...* Ecrire un programme pour dessiner un carré ..................................................* Réaliser des dessins combinant des formes différentes (triangle et carré par exemple) .......* Créer des scripts.

##**Description du déroulement de l'activité** ..............................................
* Exposé theorique/Lecture des concepts 
* Passage a la resolution des exercices
Le prof demande aux élèves d'essayer les commandes suivantes:From Turtle import; forward (120); left (90); color ("red"); forward (80); reset pour:
* Configurer la fenêtre Turtle .................................................................* Tracer et dessiner, .........................................................:...            * Se repérer et tracer ........................................................................* Dessiner des formes géométriques simples ....................................................* Dessiner des choses plus complexes ..........................................................* Colorier si nécessaire
##**Anticipation des difficultés des élèves**. ......................................................................................* Etablir une liste de pré-requis (tout ce que les élèves devraient savoir avant d’entrer dans le cours). ........................................................................................* Faire un rappel pour les élèves, à partir de la liste ci-dessus établie.........................................................................................* Soyez toujours clair avec vous-même sur ce que les élèves doivent apprendre et le chemin qu’ils doivent emprunter pour le faire...........................................................................................* Evaluer les élèves, après la séance, pour repérer leurs forces et leurs faiblesses, puis adapter votre pédagogie. 
##**Gestion de l'hétérogénéité** ..............................................................................* Pratiquer des évaluations 
différenciées..................................................................................* Créer des binômes hétérogènes................................................................* Faire des groupes de niveau.
**Analyse de l’activité**
L’objectif de cette activité est d’introduire le langage Python et surtout de familiariser les élèves à des commandes prises en charge par ledit langage.

Le professeur semble brièvement exposé le concept python : Sa syntaxe, ses commandes, sa créateur et les lieux de travail de ce dernier.

Immédiatement après l’exposé des concepts, il passe à la résolution de certains exercices d’entrainement. 
Voici comment cette activité pourrait bien se dérouler. 
 Le professeur demande aux élèves de procéder à l’analyse des commandes présentées au niveau de l’exercice No. 1 et de bien identifier la fonction de chacune de ces commandes.  
Une fois terminé, il demande à nouveau aux élèves de résoudre manuellement les 10 exercices  allant du No. 2 au No.11. Les élèves se servent alors du papier, du crayon et des instruments géométriques. Il peut aussi regrouper les élèves par deux ou par trois pour la réalisation de ces travaux.
Ces travaux étant achevés. Le professeur semble informé aux élèves qu’il existe une autre façon que vous pouvez utiliser pour faire ces travaux.  Mais dans cette deuxième façon vous travaillez avec votre ordinateur. Il leur enseigne comment configurer la fenêtre Turtle, puis, à partir des commandes vues, il leur demande de configurer cette dite fenêtre, puis d’utiliser ces commandes pour résoudre les mêmes exercices   allant du No. 2 au No.11.  Le professeur est certainement présent à leur coté pour suivre le travail de chaque élève et surtout pou aider les élèves en difficultés. 
Les élèves sont tous terminee,  il leur demande de résoudre de la même façon les exercices  allant du No. 12 au No. 16.  
Ces exercices terminés, le professeur semble  informé aux élèves qu’ils peuvent aussi utiliser des programmes ou des algorithmes pour résoudre ces exercices. Après des explications fournies aux élèves à ce sujet, le professeur leur demande de résoudre l’exercice No. 17, puis il leur demande de suivre les même démarches pour construire des triangles, des étoiles, etc. 
Enfin, il leur demande de résoudre les exercices   allant du No. 18 au No.20.  On suppose que le professeur reste en compagnie des élèves pendant tout le déroulement du travail pour superviser les élèves et aider ceux en difficultés.

Il est à dire que les futures utilisateurs de cette activité pourront toujours la modifier en ajoutant à la fiche élève d’autres points comme :
* Les notions liées
* Les matériels nécessaires
* Des exercices supplémentaires pour les élèves qui sont plus rapides;
* un quiz pour l'évaluation des acquis des élèves
* Des références ou des liens utiles; etc.

Tel est notre analyse relative à cette activité.
