

#**Thématique**
#**A la découverte des bases de données relationnelles**
##**Notions liées**
* Notion de tables
* Notions de contraintes
* Notion de références
* Projection, restitution et jointure
##**Résumée de l’activité**
Il s'agit d'une activité debranchée destinée à faire expérimenter un systeme de gestion des bases de donneés relationnelles (SGBDR), afin de se familiariser avec les concepts classiques des bases de données relationnelles.
##**Objectifs**
* Permettre aux élèves de découvrir le modèle relationnel
* Permettre aux élèves de découvrir le système de gestion des bases de données relationnelles (SGBDR).
* Permettre aux élèves de découvrir le système de langage SQL.


##**Auteur**
* Marke2006
##**Durée**
*  heures
##**Mode de participation**
* Individuel
* En groupe

##**Matériels**
* Tableau 
* Craie ou marqueur
* Papier
* Plume
##**Préparation**
* Aucune
##**Autres références**
* Fiche élèves cours
* Fiche élèves Activité, on peut le retrouver sur [Github] (https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/fiche_eleve_decouverte_BDR_charles.pdf)   

##**Analyse de l'activité**
Cette activité a été proposée par Charles Poulmaire à l’intention des élèves du niveau terminal NSI, dans le cadre du cours numérique et sciences informatiques.

C’est une activité débranchée dont l’objectif est de permettre aux élèves de découvrir le modèle relationnel, le système de gestion des bases de données relationnelles (SGBDR) et le système de langage SQL. 

Après l’énoncé des concepts, le professeur demandera aux élèves de résoudre individuellement, chacun dans son cahier ou sur son papier, l’exercice  de la Partie 1, présentant  la situation de la gestion d’emprunts de livres en anglais. 
Tout en fournissant un encadrement rapproché aux élèves de manière à les bien observer et  surtout à  aider ceux qui sont en difficultés, le professeur demandera aux élèves de répondre premièrement aux questions de la première partie de l’exercice de la partie A. 

Ces questions étant répondues, le professeur demandera aux élèves de continuer avec l’exercice de la partie A, en répondant aux questions de la deuxième partie dudit exercice.

Avec les interrogations de cette deuxième partie de l’exercice, les élèves finiront par comprendre que cette méthode de gestion, utilisant un seul tableau, qui parait comme une méthode de gestion très simple, n’est pas en réalité, une méthode de gestion simple.

L’exercice de la partie A étant achevé, le professeur, tout en fournissant le même niveau d’encadrement aux élèves, demandera à ces derniers de résoudre l’exercice de la partie B, qui utilise plusieurs tableaux en vue d’une gestion plus élaborée, en commençant par donner une réponse aux questions de la première partie de l’exercice B, puis aux questions de la deuxième partie de l’exercice B, puis aux questions de la troisième partie de l’exercice B et enfin à la  question de la quatrième partie de l’exercice B, qui permet aux élèves de faire une comparaison entre les deux méthodes de gestion mise en œuvre ( méthode avec un seul tableau et méthode avec plusieurs tableaux) dans le cadre de la gestion des bases de données relationnelles.

Donc, les élèves finiront par découvrir à ce stade de l’activité les avantages et les inconvénients de chacune de ces méthodes. 

En maintenant le même niveau d’encadrement fourni précédemment aux élèves, le professeur leur demandera de poursuivre la résolution de l’exercice partie B, en répondant aux questions de la cinquième partie de cette dite exercice. Pour ce, les élèves utilisent un langage spécifique pour obtenir automatiquement des réponses  à partir des tableaux. Pour les questions  5(a), les élèves utiliseront des requêtes toutes faites pour effectuer leurs recherches tandis que pour les questions 5(b) les élèves écrivent d’abord leurs requêtes  avant d’effectuer leurs recherches.
