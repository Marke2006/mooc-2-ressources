
Nous tenons à vous informer que ne pouvons pas finaliser l’activité proposée, car nous n’avons pu récupérer et lire :
•	L'activité élève proprement dite ;
•	Le poster collaboratif ;
•	Le poster collaboratif corrigé ;
•	Le poste collaboratif "la totale" ;
•	et La grille permettant de construire le poster collaboratif 

Nos recherches sur le site indiqué  se sont révélées vaines et sans succès.  Vous pourrez toujours nous proposer un autre exercice si vous le jugerez nécessaire.
